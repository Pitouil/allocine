<?php

namespace App\Form;

use App\Entity\Actor;
use App\Entity\Composer;
use App\Entity\Director;
use App\Entity\Movie;
use Doctrine\DBAL\Types\TextType;
use Doctrine\ORM\Mapping\Entity;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Unique;

class MovieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, [
//                'constraints' => [
//                    new Unique([
//                        'message' => 'You can\'t have two movie with the same title'
//                    ])
                ])

            ->add('releaseDate')
            ->add('synopsis')
            ->add('actors', EntityType::class,[
                'class' => Actor::class,
                'multiple'=> true,
                'expanded' => true,
            ])
            ->add('directors', EntityType::class,[
                'class' => Director::class,
                'multiple'=> true,
                'expanded' => true,
            ])
            ->add('composer', EntityType::class,[
                'class' => Composer::class,
                'multiple'=> false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Movie::class,
        ]);
    }
}
